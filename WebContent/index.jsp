<%-- 
    Document   : index2
    Created on : Apr 7, 2015, 5:14:31 PM
    Author     : Christopher
--%>

<%@page import="test.Cart"%>
<%@page import="test.DatabaseUtility"%>
<%@ page import="test.Product" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,java.util.*,javax.naming.*" %>
<%@ include file="DB_CONNECTION.jsp" %>
<jsp:include page="header.jsp" />
<%
	if (session.getAttribute("cart") == null) {
	    Cart cart = new Cart(DB_CONNECTION_STRING);
	    session.setAttribute("cart", cart);
	}
    
	String name = (String) session.getAttribute("username");
	
	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		session.setAttribute("login_errors", "No user logged in");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", "login.jsp"); 
	} else {
		session.setAttribute("login_errors", "");
	}
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
    </head>
    <body>
			<ul>
            <%if (session.getAttribute("role") != null && session.getAttribute("role").equals("owner")) { %>
              <li><a href="Categories.jsp">Categories</a></li>
              <li><a href="products.jsp">Products</a></li>
              <%}%>
			  <li><a href="index.jsp">Home</a></li>
              <li><a href="productbrowser.jsp">Browse</a></li>
              <li><a href="buycart.jsp">Cart</a></li>
            </ul>
    </body>
</html>
<% conn.close(); %>
<%
dbUtility.done();
%>