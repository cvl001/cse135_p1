<%-- 
    Document   : buycart
    Created on : Apr 19, 2015, 9:52:33 AM
    Author     : Christopher
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="test.Cart"%>
<%@page import="test.Product"%>
<%@ include file="DB_CONNECTION.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% String name = (String) session.getAttribute("username");
	
	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		session.setAttribute("login_errors", "No user logged in");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", "login.jsp"); 
	   
	} %>
<jsp:include page="header.jsp" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <p>Cart Items</p>
        <table>
                <tr>
                        <th>Product Name</th>
                        <th>Product SKU</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Price</th>
                </tr>

                <%
                 	if(session.getAttribute("cart") == null){
                		Cart cart = new Cart(DB_CONNECTION_STRING);
                		session.setAttribute("cart", cart);
                	}
                    ArrayList<Product> prods = ((Cart)session.getAttribute("cart")).cart;
                    float total = 0;
                    if (prods != null) {
                    for (int i = 0 ; i < prods.size() ; i++) {
                        total += prods.get(i).qty * prods.get(i).price;
                %>
                
                <tr>
                    <td><%=prods.get(i).name%></td>
                    <td><%=prods.get(i).sku%></td>
                    <td><%=prods.get(i).category%></t>
                    <td><%=prods.get(i).qty%></td>
                    <td>$<%=String.format("%.2f", prods.get(i).price)%></td>
                </tr>
                
                <%}}%>
                <!-- Starting the update portion  -->
        </table>
                <br/>
                Total $<%=String.format("%.2f", total)%>
                <br/>

                <form name="productviewer" action="purchaseconfirmation.jsp" method="post">
                    Credit Card Number: <input type="text" name="credit_num" />
                    <input type="submit" value="Purchase"/>
                </form>
    </body>
</html>
