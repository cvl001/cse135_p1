<%-- 
    Document   : Categories
    Created on : Apr 19, 2015, 3:20:24 PM
    Author     : heatherhua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="DB_CONNECTION.jsp" %>
<%@include file="header.jsp" %>
<%@page import="test.DatabaseUtility"%>
<!DOCTYPE html>
<html>
    <%
	    String role = (String) session.getAttribute("role");
    	String ownerView;
    	String userView;
    	if(role != null && !role.isEmpty()){
                ownerView = (role.equals("owner"))?"block":"none";
                userView = (role.equals("customer"))?"block":"none";
        } else {
        	// Should never get here
        	ownerView = "";
        	userView = "";
        }
        String update_string;
		
		ResultSet result = dbUtility.getAllCategories();
        
        String onEdit = request.getParameter("Edit");
        String onSave = request.getParameter("Save");
		String categoryName = request.getParameter("Name");
		String categoryDesc = request.getParameter("Description");
		String delete = request.getParameter("delete");
        
        String mode = "";
        if(request.getParameter("mode") != null)
            	mode = request.getParameter("mode").toString();
            
        switch(mode) {
        	case "Create" : 
        		System.out.println("Creating: " + categoryName);
        		try {
        			dbUtility.createCategory(categoryName, categoryDesc);
        			session.setAttribute("errors", "");
        			System.out.println("...Finished creating " + categoryName);
        			session.setAttribute("success","Successfully created category");
        		}
        		catch (Exception e){
        			System.out.println("...Could not create " + categoryName);
					session.setAttribute("errors", "Data Modification Failure. ");
        		}
				mode = "";	
        		response.sendRedirect(request.getContextPath() + "/Categories.jsp");
        		break;
        	case "Delete" :
         		try { 
         			dbUtility.deleteCategory(delete);
        			session.setAttribute("errors", "");
        			session.setAttribute("success","Successfully deleted category");
         		} catch (Exception e) {
					session.setAttribute("errors", "Data Modification Failure. ");
         		}
				mode = "";	
         		response.sendRedirect(request.getContextPath() + "/Categories.jsp");
        		break;       		
        	default:
        			session.setAttribute("errors", "");        		
					session.setAttribute("success", "");
        }
        if(onEdit != null){
        	try {
        		if(!dbUtility.checkCategoryExists(onEdit)){
    				mode = "";	
    				session.setAttribute("errors", "Too slow! Category has been deleted!");
             		response.sendRedirect(request.getContextPath() + "/Categories.jsp");
        		}
        	}catch (Exception e){}
        }
        if(onSave != null){
            categoryName = request.getParameter("UpdateName");
            categoryDesc = request.getParameter("UpdateDescription");
            
        	try {
        		dbUtility.updateCategory(onSave, categoryName, categoryDesc);
    			session.setAttribute("success","Successfully updated category");
        	} catch (Exception e) {
                    session.setAttribute("errors", "Data Modification Failure. ");
        	}
        	
            response.sendRedirect(request.getContextPath() + "/Categories.jsp");
        }
	%>

    <head>
		<link rel="stylesheet" type="text/css" href="css/style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Categories Page</title>
    </head>
    <body> 
    <div style="display:<%= userView %>">
        <h4>This page is available to owners only.</h4>
    </div>
        
    <div style="display:<%= ownerView%>; margin-left: auto; margin-right:auto;">
    
    <!-- ******************* HTML CODE STARTS NOW ***********************-->

	<!--  DISPLAY EXISITING CATEGORIES-->
	<table>
	    <tr>
		<th>Name</th>
		<th>Description</th>
		<th>Product Count</th>
		<th>Edit</th>
		<th>Delete</th>
	    </tr>
	    <% 
            while(result.next()) {%>
	    <tr>
            <%
                name = result.getString(1);
                String desc = result.getString(2);
                
                if(result.getString(1).equals(onEdit)){
                    name = "<input type=\"text\" name=\"UpdateName\" value="+onEdit+">";
                    desc = "<input type=\"text\" name=\"UpdateDescription\" value=\""+desc+"\">";
                    update_string = "Save";
                }else{
                    update_string = "Edit";
                }
            %>
            <form action="<%=request.getContextPath()%>/Categories.jsp?<%=update_string%>=<%=result.getString(1)%>" 
                  method="post">
            	<td><%= name %></td>
            	<td><%= desc %></td>
            	<td><%= result.getString(3) %></td>
            	<td>
                	<input type="submit" value="<%= update_string %>">
	            </td>
            </form>
            
            <% 
            String delete_display;
            if (result.getString(3).equals("0")){
                delete_display="block";
            } else{
                delete_display = "none";
            }%>
            <td>
              <form action="<%=request.getContextPath()%>/Categories.jsp?mode=Delete&delete=<%=result.getString(1)%>" method="post">    
                <input type="submit" value="Delete" style="display:<%=delete_display%>">
              </form>
            </td>
	    </tr>
	    <% 
          } %>
	    <tr>
		<form action="<%=request.getContextPath()%>/Categories.jsp?mode=Create" method="post">
		    <td><input type="text" name="Name"></td>
		    <td><input type="text" name="Description"></td>
		    <td><input type="submit" name="Create"></td>
		</form>
	    </tr>
    </table>
	
    </div>
    </body>
</html>

<%
dbUtility.done();
%>
