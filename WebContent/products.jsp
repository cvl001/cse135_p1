<%-- 
    Document   : products
    Created on : Apr 20, 2015, 7:48:39 PM
    Author     : heatherhua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<%@ include file="DB_CONNECTION.jsp" %>
<%@page import="test.DatabaseUtility"%>

<!DOCTYPE html>
<html>
    <head>
		<% 	
			String role = (String) session.getAttribute("role");
			String ownerView;
			String userView;
			if(role != null && !role.isEmpty()){
		    	ownerView = (role.equals("owner"))?"block":"none";
		    	userView = (role.equals("customer"))?"block":"none";
			}else {
				//Should never get here.
				ownerView = "";
				userView = "";
			} 
			String uri = request.getRequestURI();
			String search = request.getParameter("search");
			String pageName = uri.substring(uri.lastIndexOf("/")+1);
			if (search == null || search.equals("null"))
				search = "";
			
            // Create variables
            String newName = request.getParameter("productName");
            String newCategory = request.getParameter("productCategory");
			String newSKU = request.getParameter("productSKU");
            String newPrice = request.getParameter("productPrice");
            
            // Update variables
			String updateName = request.getParameter("pName_");
        	String updateCategory = request.getParameter("pCategory_");
			String updateSKU = request.getParameter("pSKU_");
        	String updatePrice = request.getParameter("pPrice_");
			String orig_sku = request.getParameter("oSKU_");

            ResultSet categorySet = dbUtility.getAllCategories();
            String categoryName = request.getParameter("category");
            String searchString = request.getParameter("search");
            ResultSet product_results;
            String mode = "";
            
            if(request.getParameter("mode") != null)
            	mode = request.getParameter("mode").toString();
            
           	System.out.println("MODE: " + mode ); 
 			switch(mode) {
 				case "Create":
 					try {
		            	dbUtility.createProduct(newName, newSKU, newCategory, newPrice);
		            	session.setAttribute("errors", "");
		            	session.setAttribute("success","Successfully created Product Name:" + newName + " SKU:" + newSKU + " Category:" + newCategory + " Price:$" + newPrice );
		            } catch (Exception e) {
						session.setAttribute("errors", "Create failure. " + e.getMessage());
		            }
		            	
                	response.sendRedirect(request.getContextPath() + "/products.jsp?category="+categoryName + "&search=" + searchString);
		            break;
 				case "Update":            
 					try {
 						dbUtility.updateProduct(orig_sku, updateName, updateSKU, updateCategory, updatePrice);
		            	session.setAttribute("errors", "");
		            	session.setAttribute("success","Successfully updated Product Name:" + updateName + " SKU:" + updateSKU + " Category:" + updateCategory + " Price:$" + updatePrice );
 					}
		            catch (Exception e) {
						session.setAttribute("errors", "Update Failure. " + e.getMessage());
		            }
                	response.sendRedirect(request.getContextPath() + "/products.jsp?category="+categoryName + "&search=" + searchString);
 					break;
 				case "Delete": 
		            String deleteSku = request.getParameter("deleteSku");
 					try {
 						dbUtility.deleteProduct(deleteSku);
 						session.setAttribute("errors", "");
		            	session.setAttribute("success","Successfully deleted product with SKU:" + deleteSku);
 					}
		            catch (Exception e) {
						session.setAttribute("errors", "Delete Failure. " + e.getMessage());
		            }
                	response.sendRedirect(request.getContextPath() + "/products.jsp?category="+categoryName + "&search=" + searchString);
 					break;
 				default: 
					session.setAttribute("errors", "");
					session.setAttribute("success", "");
 					
 			}
        %>
        <link rel="stylesheet" 
                  type="text/css" 
                  href="css/style.css">
            <meta http-equiv="Content-Type" 
                  content="text/html; charset=UTF-8">
            
        <title>Products Page</title>
    </head>
    <body>
      <!-- USER VIEW -->
      <div style="display:<%= userView %>">
        <h4>This page is available to owners only.</h4>
      </div>
      <!-- OWNER VIEW -->
      <div style="display:<%= ownerView%>; 
        margin-left: auto; 
        margin-right:auto;">
        <ul id="categories">
        	<li><a href="<%=pageName%>?search=<%=request.getParameter("search")%>">All</a></li>        	
        	
        	<%
        		ResultSet categories = dbUtility.getAllCategories();
        		while(categories.next()) {	
        	%>
        	
        	<li><a href="<%=pageName%>?category=<%=categories.getString(1)%>&search=<%=request.getParameter("search")%>"><%=categories.getString(1)%></a></li>        	
        	<%}%>
        </ul>
        <table class="search">
            <form action="<%=pageName%>" method="GET"> 
            <tr>
                <td>
                   <input type="hidden" name="category" value="<%=request.getParameter("category")%>">
                   <input type="search" name="search" value="<%=search%>">
                </td>
            </tr>
            </form>
        </table>
        <!-- Products Table -->
        <form action="<%=request.getContextPath()%>/products.jsp?mode=Create" method="post">
        <input type="hidden" name="category" value="<%=request.getParameter("category") %>">
        <input type="hidden" name="search" value="<%=request.getParameter("search") %>">
        <table class="products">
          <tr>
            <th>Name</th>
            <th>SKU</th>
            <th>Category</th>
            <th>Price</th>
            <th></th>
            <th></th>
          </tr>
          <!-- CREATE NEW PRODUCT -->
          <tr>
            <td><input type="text" name="productName"></td>
            <td><input type="text" name="productSKU"></td>
            <td>
              <select name="productCategory">
                <% 
            		categorySet = dbUtility.getAllCategories();
                	while(categorySet.next()){ %>
                  <option value=<%=categorySet.getString(1)%>> <%=categorySet.getString(1)%></option>
                <% } %>
              </select> 
            </td>
            <td><input type="text" name="productPrice"></td>
            <td>
               <input type="submit" name="submitMode" value="Create"></td>
             <td></td>
             <td></td>
          </tr>
          </form>
          
          <!-- DISPLAY PRODUCTS  -->
        	<%  
	           	product_results = dbUtility.getAllProducts(categoryName, request.getParameter("search"));
        		while(product_results.next()){ 
        	%>
	          <tr>
	 			<form action="<%=request.getContextPath()%>/products.jsp?mode=Update" method="post">
		        <input type="hidden" name="category" value="<%=request.getParameter("category") %>">
   				<input type="hidden" name="search" value="<%=request.getParameter("search") %>">
	           	<td><input type="text" name="pName_" value="<%=product_results.getString(2)%>"></td> 
	          	<td><input type="text" name="pSKU_" value="<%=product_results.getString(3)%>"></td>
				<td><select name="pCategory_">
			            <option value=<%= product_results.getString(4) %>><%=product_results.getString(4)%></option>
			            <% 
	            			categorySet = dbUtility.getAllCategories();
			            	while(categorySet.next()){  
			            %>
						<option value=<%=categorySet.getString(1)%>> <%=categorySet.getString(1)%></option>		                  
			            <% } %>
					</select></td>
	          	<td><input type="text" name="pPrice_" value="<%=product_results.getString(5)%>"></td>
	          	<input type="hidden" name="oSKU_" value="<%=product_results.getString(3)%>">
	            <td>
	            	<input type="submit" value="Update"></td>
	            </form>
	            <td>
  		      		<form action="<%=request.getContextPath()%>/products.jsp?mode=Delete&deleteSku=<%=product_results.getString(3)%>" method="post">
	      		        <input type="hidden" name="category" value="<%=request.getParameter("category") %>">
	   					<input type="hidden" name="search" value="<%=request.getParameter("search") %>">
	            		<input type="submit" name="submitMode" value="Delete">
	            	</form> 
	        	</td>
       		</tr>
       		<%}%>
      	</table>
      </div>
    </body>
</html>

<%
conn.close();
dbUtility.done();
%>
