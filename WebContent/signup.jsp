<%-- 
    Document   : signup
    Created on : Apr 16, 2015, 8:54:32 PM
    Author     : Kelly-Mac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="DB_CONNECTION.jsp" %>
<%@ page import ="java.sql.*" %>
<%
    String name = request.getParameter("name");
	name = name.trim();
	String state = request.getParameter("state");
	String age = request.getParameter("age");
	String role = request.getParameter("role");
	System.out.println(name + " " + age + " " + role + " " + state);

	if(name.equals("")) {
		out.println("No name entered <a href='signup.html'>try again</a>");
	}
	else if(state.equals("")) {
		out.println("No state entered <a href='signup.html'>try again</a>");
	}
	else if(age.equals("") || Integer.parseInt(age) < 0 ) {
		out.println("No age entered/age must be positive <a href='signup.html'>try again</a>");
	}
	else {
    Statement st = conn.createStatement();
    try {
    st.executeUpdate("INSERT INTO users VALUES ('" + name + "', '" + age + "', '" + state + "')");
    if(role.equals("owner"))
    	st.executeUpdate("INSERT INTO owners VALUES('" + name + "')");
    else
    	st.executeUpdate("INSERT INTO customers VALUES('" + name + "')");
    out.println("Signup Success! <a href='index.jsp'>Go to Home Page</a>");
    session.setAttribute("username", name);
    session.setAttribute("role", role);
    }
    catch (Exception e) {
    	System.out.println(e.toString());
   		String error = e.toString();
   		if(error.contains("violates unique constraint 'users_pkey'"));
   			out.println("Name already taken <a href='signup.html'>try again</a>");
   			
   		
    }
	}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    </body>
</html>
<% conn.close(); %>