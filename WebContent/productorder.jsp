<%-- 
    Document   : ProductBrowser
    Created on : Apr 17, 2015, 9:53:10 AM
    Author     : Christopher
--%>

<%@page import="test.Cart"%>
<%@page import="test.DatabaseUtility"%>
<%@ page import="test.Product" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*,java.util.*,javax.naming.*" %>
<%@ include file="DB_CONNECTION.jsp" %>
<jsp:include page="header.jsp" />

<%
	String name = (String) session.getAttribute("username");
	
	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		response.setStatus(response.SC_MOVED_TEMPORARILY);
	    response.setHeader("Location", "login.jsp"); 
	}	
	if (request.getParameter("name") == null){
		String redirectURL = "index.jsp";
		session.setAttribute("errors", "Invalid request.");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
	    response.setHeader("Location", "index.jsp"); 	
	}
	
    if (request.getParameter("redirect") != null && request.getParameter("redirect").equals("true")) {
        Cart cart = (Cart)session.getAttribute("cart");
        try {
        	cart.addToCart(new Product(request));
        	conn.close();
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", "productbrowser.jsp"); 
        } catch (Exception e) {
			session.setAttribute("errors", "Invalid data! " + e.getMessage());
			String URL = request.getContextPath() + "/productorder.jsp?";
			URL += "name=" + request.getParameter("name");
			URL += "&sku=" + request.getParameter("sku");
			URL += "&category=" + request.getParameter("category");
		    URL += "&qty=" + request.getParameter("qty");
		    URL += "&price=" + request.getParameter("price");
		    URL += "&id=" + request.getParameter("id");
		    
		    conn.close();
            response.sendRedirect(URL); 
        }
    } else {
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        	
        <p>Cart Items</p>
        <table>
                <tr>
                        <th>Product Name</th>
                        <th>Product SKU</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Price</th>
                </tr>

                <%  
                	if(session.getAttribute("cart") == null){
                		Cart cart = new Cart(DB_CONNECTION_STRING);
                		session.setAttribute("cart", cart);
                	}
                	ArrayList<Product> prods = ((Cart)session.getAttribute("cart")).cart;
    
                    if (prods != null) {
                    for (int i = 0 ; i < prods.size() ; i++) {
                    
                %>
                
                <tr>
                    <td><%=prods.get(i).name%></td>
                    <td><%=prods.get(i).sku%></td>
                    <td><%=prods.get(i).category%></td>
                    <td><%=prods.get(i).qty%></td>
                    <td>$<%=String.format("%.2f", prods.get(i).price)%></td>
                </tr>
                
                <%}}%>
                <!-- Starting the update portion  -->
        </table>
        <br />
        <table>
        	Add following item to cart?
                <tr>
                        <th>Product Name</th>
                        <th>Product SKU</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th></th>
                </tr>
                <tr>
                    <form name="productpurchase" action="productorder.jsp" method="get">
                        <input type="hidden" name="redirect" value="true">
                        <input type="hidden" value="<%=request.getParameter("name")%>" name="name">
                        <input type="hidden" value="<%=request.getParameter("sku")%>" name="sku">
                        <input type="hidden" value="<%=request.getParameter("category")%>" name="category">
                        <input type="hidden" value="<%=request.getParameter("price")%>" name="price">
                        <input type="hidden" value="<%=request.getParameter("id")%>" name="id">
                        
                        <td><%=request.getParameter("name")%></td>
                        <td><%=request.getParameter("sku")%></td>
                        <td><%=request.getParameter("category")%></td>
                        <td><input type="text" name="qty"></td>
                        <td>$<%=String.format("%.2f", Float.parseFloat(request.getParameter("price") == null ? "0" : request.getParameter("price")))%></td>
                        <td><input type="submit"></td>
                    </form>
                </tr>
        </table>
    </body>
</html>
<%}%>
<% conn.close(); %>
