<%@ include file="DB_CONNECTION.jsp" %>
<%@page import="test.Cart"%>
<%@page import="test.DatabaseUtility"%>
<%@ page import="test.Product" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
	String uri = request.getRequestURI();
	String pageName = uri.substring(uri.lastIndexOf("/")+1);
	String search = request.getParameter("search");

	if (search == null || search.equals("null"))
		search = "";
%>  
		
        <ul id="categories">
        	<li><a href="<%=pageName%>?search=<%=request.getParameter("search")%>">All</a></li>        	
        	
        	<%
        		ResultSet categories = dbUtility.getAllCategories();
        		while(categories.next()) {	
        	%>
        	
        	<li><a href="<%=pageName%>?category=<%=categories.getString(1)%>&search=<%=request.getParameter("search")%>"><%=categories.getString(1)%></a></li>        	
        	<%}%>
        </ul>
        <table class="search">
            <form action="<%=pageName%>" method="GET"> 
            <tr>
                <td>
                   <input type="hidden" name="category" value="<%=request.getParameter("category")%>">
                   <input type="search" name="search" value="<%=search%>">
                </td>
            </tr>
            </form>
        </table>
</body>
</html>
<% conn.close(); %>