<%-- 
    Document   : buycart
    Created on : Apr 19, 2015, 9:52:33 AM
    Author     : Christopher
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="test.Cart"%>
<%@page import="test.Product"%>
<%@include file="DB_CONNECTION.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp" />
<% 
	String name = (String) session.getAttribute("username");
	
	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		session.setAttribute("login_errors", "No user logged in");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
	    response.setHeader("Location", "login.jsp"); 
	} else 
		session.setAttribute("login_errors", "");
	
   	if(session.getAttribute("cart") == null){
   		Cart cart = new Cart(DB_CONNECTION_STRING);
   		session.setAttribute("cart", cart);
   	}
    Cart cart = (Cart)session.getAttribute("cart");
    int product_id = -1;
    try {
    	product_id = cart.purchase((String)session.getAttribute("username"));
    } catch (Exception e) {
    	session.setAttribute("errors", "Purchase error. " + e.getMessage());
    }
    if (product_id == -1) {
		response.sendRedirect(request.getContextPath() + "/buycart.jsp");
    } else {

    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
<body>
		<% if (product_id != -1) { %>
			
		<p>Purchase successful! Your Product Number is: <%=product_id %></p>
        <p>Items purchased</p>
        <table>
                <tr>
                        <th>Product Name</th>
                        <th>Product SKU</th>
                        <th>Category</th>
                        <th>Quantity</th>
                        <th>Price</th>
                </tr>

                <%
              
                    ArrayList<Product> prods = ((Cart)session.getAttribute("cart")).cart;
                    int total = 0;
                    if (prods != null) {
                    for (int i = 0 ; i < prods.size() ; i++) {
                        total += prods.get(i).qty * prods.get(i).price;
                %>
                
                <tr>
                    <td><%=prods.get(i).name%></td>
                    <td><%=prods.get(i).sku%></td>
                    <td><%=prods.get(i).category%></td>
                    <td><%=prods.get(i).qty%></td>
                    <td>$<%=prods.get(i).price%></td>
                </tr>
                
                <%}}%>
                <!-- Starting the update portion  -->
                <%	cart.reset(); %>
        </table>
        <%}%>
        
    </body>
</html>
<%     
	dbUtility.done();
%>
