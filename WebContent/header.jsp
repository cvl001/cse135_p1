<%-- 
    Document   : header
    Created on : Apr 19, 2015, 10:06:56 AM
    Author     : Christopher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%	String name = (String) session.getAttribute("username");

	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		session.setAttribute("login_errors", "No user logged in");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", "login.jsp"); 
	}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--         		<div style="display:true; 
             margin-left: auto; 
             margin-right:auto;"> -->
            <link rel="stylesheet" 
                  type="text/css" 
                  href="css/style.css">
            <meta http-equiv="Content-Type" 
                  content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        	<h4><%=session.getAttribute("errors") != null ? session.getAttribute("errors") : ""%></h4>
        	<h4><%=session.getAttribute("success") != null ? session.getAttribute("success") : ""%></h4>
        	<%
        		session.setAttribute("errors", "");
    			session.setAttribute("success", "");
        	%>
    </head>
    <body>
        <h1>Hello <%=name %>!</h1>
        <%if (session.getAttribute("role") != null && session.getAttribute("role").equals("customer")) { %>
        	<a href="buycart.jsp" style="text-align: right">Buy Cart</a>
       	<%}%>
		<div class="top"> <!-- Filter navbar by role-->
            <ul class="nav">
              <li><a href="Categories.jsp">Categories</a></li>
              <li><a href="products.jsp">Products</a></li>
			  <li><a href="index.jsp">Home</a></li>
              <li><a href="productbrowser.jsp">Browse</a></li>
              <li><a href="buycart.jsp">Cart</a></li>
            </ul>
        </div>
    </body>
</html>
