<%-- 
    Document   : ProductBrowser
    Created on : Apr 17, 2015, 9:53:10 AM
    Author     : Christopher
--%>

<%@ page import="java.sql.*,java.util.*,javax.naming.*" %>
<%@ include file="hello_name.jsp" %>
<%@ include file="product_header.jsp" %>
<%
	String name = (String) session.getAttribute("username");
	
	if (name == null || name.equals("")) {
		String redirectURL = "login.jsp";
		session.setAttribute("login_errors", "No user logged in");
		response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", "login.jsp"); 
	}else {
		session.setAttribute("login_errors", "");
	}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<div style="display:true; 
             margin-left: auto; 
             margin-right:auto;">
            <link rel="stylesheet" 
                  type="text/css" 
                  href="css/style.css">
            <meta http-equiv="Content-Type" 
                  content="text/html; charset=UTF-8">
        <title>JSP Page</title>        
    </head>
    <body>     
    <a href="buycart.jsp" style="text-align: right">Buy Cart</a>
        <table>
                <tr>
                        <th>Product Name</th>
                        <th>Product SKU</th>
                        <th>Category</th>
                        <th>Price</th>
                </tr>

                <!-- Starting the insert portion  -->
                <%
                
                	ResultSet products = dbUtility.getAllProducts(request.getParameter("category"), request.getParameter("search"));
    
                    if (products != null) {
                    while (products.next()) {
                        
                    
                %>
                
                <tr>
                <form name="productviewer" action="productorder.jsp" method="get">
                    <input type="hidden" value="<%=products.getString("name")%>" name="name">
                    <input type="hidden" value="<%=products.getInt("SKU")%>" name="sku">
                    <input type="hidden" value="<%=products.getString("category")%>" name="category">
                    <input type="hidden" value="<%=products.getFloat("price")%>" name="price">
					<input type="hidden" value="<%=products.getInt("id")%>" name="id">
					
                    <td><input type="submit" value="<%=products.getString("name")%>"></td>
                    <td><%=products.getInt("SKU")%></td>
                    <td><%=products.getString("category")%></td>
                    <td>$<%=products.getString("price")%></td>
                </form>
                </tr>
                
                <%}}%>
                <!-- Starting the update portion  -->
        </table>
    </body>
</html>
<% conn.close(); %>
<%
dbUtility.done();
%>