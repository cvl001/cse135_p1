package test;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

/**
 *
 * @author Christopher
 */
public class DatabaseUtility {
    Connection conn;
    public DatabaseUtility(String conn_string) {
        try {
            Class.forName("org.postgresql.Driver"); 
            conn = DriverManager.getConnection(conn_string,"superuser","Superuser1");
            
        } catch (Exception e) {
                System.out.println(e.toString());
        }
    }
    
    public boolean deleteProduct(String sku) throws Exception { 
        PreparedStatement pstmt = conn.prepareStatement("DELETE from products WHERE sku = ?");
        try {
   			conn.setAutoCommit(false);
            int sku_ = Integer.parseInt(sku); 	
            pstmt.setInt(1, sku_);
            int num = pstmt.executeUpdate();
            if (num < 1) {
            	throw new Exception("Product no longer exists!"); 
            }
           // pstmt.execute();
            updateProductCount();
            return true;
        } catch (Exception e) { 
        	throw e;
        }finally {
		    if (pstmt != null){
			pstmt.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}        
    }
    
    private void updateProductCount() throws Exception {
    	    PreparedStatement updateCategory = conn.prepareStatement("UPDATE categories SET product_count = (SELECT count(category) FROM products WHERE products.category = categories.name);");
            updateCategory.execute();
    }
    
    public boolean updateProduct(String original_sku, String name, String new_sku, String category, String price) throws Exception {
    	if (name == null || name.isEmpty() || name.trim().length() == 0)
    		throw new Exception("Name cannot be empty!");
    	
    	if (category == null || category.isEmpty())
    		throw new Exception("Category cannot be empty!");
    	
    	if (new_sku == null || new_sku.isEmpty())
    		throw new Exception("SKU cannot be empty!");
    	
    	if (price == null || price.isEmpty())
    		throw new Exception("Price cannot be empty!");
        
    	PreparedStatement pstmt = conn.prepareStatement("UPDATE products SET name=?, sku=? , category=?,price=? WHERE sku =?");
        try {
        	name = name.trim();
   			conn.setAutoCommit(false);
        	int sku_ = Integer.parseInt(new_sku);
        	int orig_sku_ = Integer.parseInt(original_sku);
        	
    		float price_ = Float.parseFloat(price);
            
            pstmt.setString(1, name);
            pstmt.setInt(2, sku_);
            pstmt.setString(3,category);
            pstmt.setFloat(4, price_);
            pstmt.setInt(5, orig_sku_);
            
            int num = pstmt.executeUpdate();
            if (num < 1) {
            	throw new Exception("Product no longer exists!"); 
            }
            updateProductCount();
            return true;
        } catch (Exception e) { 
			if (e.getMessage().contains("violates foreign key constraint")) {
				throw new Exception("Chosen category no longer exists!");
			} else if (e.getMessage().contains("duplicate key value violates")) {
				throw new Exception("SKU already exists!");
			} else if (e.getMessage().contains("Product no longer exists!")) {
				throw e;
			} else if(e.getMessage().contains("violates check constraint \"products_price_check\"")) {
				throw new Exception("Prices must be valid!");
			} else {
				throw e;
			}
			
        }finally {
		    if (pstmt != null){
			pstmt.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}
    }
    
    public boolean checkCategoryExists(String category) throws SQLException {
    	ResultSet res = conn.createStatement().executeQuery("SELECT * FROM categories WHERE name = '" + category +"'");
		return res.next();
    }
    public boolean createProduct(String name, String sku, String category, String price) throws Exception {
    	
    	if (name == null || name.isEmpty() || name.trim().length() == 0)
    		throw new Exception("Name cannot be empty!");
    	
    	if (category == null || category.isEmpty())
    		throw new Exception("Category cannot be empty!");
    	
    	if (sku == null || sku.isEmpty())
    		throw new Exception("SKU cannot be empty!");
    	
    	if (price == null || price.isEmpty())
    		throw new Exception("Price cannot be empty!");
    	if (Float.parseFloat(price) < 0)
    		throw new Exception("Price cannot be less then $0.00");
    	if (Integer.parseInt(sku) < 0)
    		throw new Exception("SKU must be positive");

    		int sku_ = Integer.parseInt(sku);
    		PreparedStatement p = conn.prepareStatement("INSERT into products (name,sku,category,price) values(?,?,?,?)");
    		try {
    			name = name.trim();
    			conn.setAutoCommit(false);
	    		
	    		float price_ = Float.parseFloat(price);
	    		
	    		p.setString(1, name);
	    		p.setInt(2, sku_);
	    		p.setString(3, category);
	    		p.setFloat(4, price_);
	    		p.execute();            
	    		updateProductCount();
    		} catch (Exception e) {
    			System.out.println(e);
    			if (e.getMessage().contains("violates foreign key constraint")) {
    				throw new Exception("Chosen category no longer exists!");
    			} else if (e.getMessage().contains("duplicate key value violates")) {
    				throw new Exception("SKU already exists!");
    			}
    		}finally {
		    if (p != null){
			p.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}
    	return true;
    		
    }
    
    public void updateProductCount(String category) throws SQLException {
		PreparedStatement p = conn.prepareStatement("UPDATE CATEGORIES SET product_count = product_count + 1 WHERE name = ?");
		p.setString(1, category);
		p.execute();
    }
    
    public boolean createCategory(String name, String desc) throws Exception {
        
    	if (name == null || name.equals("") || name.trim().length() == 0) {
    		throw new Exception ("Name cannot be empty");
    	}
    	
    	if (desc == null || desc.equals("") || desc.trim().length() == 0) {
    		throw new Exception ("Description cannot be empty");
    	}

        String create_query = "INSERT into categories VALUES (?,?,0)";
        PreparedStatement p = conn.prepareStatement(create_query);
		try {
			desc = desc.trim();
			name = name.trim();
		    conn.setAutoCommit(false);
		    p.setString(1, name);
		    p.setString(2, desc);
		    p.execute();
		    return true;
		}catch(Exception e){
		    if(conn != null){
			conn.rollback();
		    }
		    if (e.getMessage().contains("duplicate key value violates unique constraint"))
		    	throw new Exception("Category name already exists!");
		}finally {
		    if (p != null){
			p.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}
		return false;
    }
    
    public ResultSet getAllProducts(String category,String product) {
        ResultSet rs = null;
        
        if (category == null || category.equals("null") || category.trim().length() == 0)
            category = "%";
        
        if (product == null || product.equals("null") || product.trim().length() == 0)
            product = "";
        
        product = product.trim();
        category = category.trim();
        try {
            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM products JOIN categories ON products.category = categories.name WHERE categories.name LIKE ? AND products.name LIKE ?");
            pstmt.setString(1, category);
            pstmt.setString(2, "%" + product + "%");
            rs = pstmt.executeQuery();
        } catch (Exception e) { 
        	
        	System.out.println("im confused");
        }
        
      return rs;
    }
        
    public boolean deleteCategory(String name) throws Exception {
        if (name == null || name.isEmpty())
            throw new Exception("hella goof");
        PreparedStatement pstmt = conn.prepareStatement("DELETE from categories WHERE name = ? AND product_count = 0");
        try {        	
		    conn.setAutoCommit(false);
            pstmt.setString(1, name);
            int deleted = pstmt.executeUpdate();
            if(deleted < 1)
            	throw new Exception("Category no longer exists!");
            return true;
        } catch (Exception e) {
        	throw e;
        }finally {
		    if (pstmt != null){
			pstmt.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}
    }
    public boolean updateCategory(String original_name, String new_name, String new_description) throws Exception {
        if (original_name == null || new_name == null || new_name.isEmpty() || new_name.trim().length() == 0)
        	throw new Exception("Name cannot be empty!");
        
        if (new_description == null || new_description.isEmpty() || new_description.trim().length() == 0)
            throw new Exception("Description cannot be empty");
        
        PreparedStatement pstmt = conn.prepareStatement("UPDATE categories SET name=?, description=? WHERE name = ?");
        try {
        	new_name = new_name.trim();
        	new_description = new_description.trim();
        	
		    conn.setAutoCommit(false);
            
            pstmt.setString(1, new_name);
            pstmt.setString(2, new_description);
            pstmt.setString(3,original_name);

            int num = pstmt.executeUpdate();
            
            if (num < 1)
            	throw new Exception("Category has been deleted!");
            return true;
        } catch (Exception e) {
        	if (e.getMessage().contains("duplicate key value violates unique constraint")) {
        		throw new Exception("Category already exists!");
        	}
        	throw e;
        }finally {
		    if (pstmt != null){
			pstmt.close();
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}
    }
    
    public void done() throws SQLException {
    	conn.close();
    }
    public ResultSet getAllCategories() {
        try {
            PreparedStatement pstmt = conn.prepareStatement("SELECT * from categories ORDER BY name");

            return pstmt.executeQuery();
        } catch (Exception e) { }
        
      return null;
    }
}
