package test;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Christopher
 */
public class Cart {
    public ArrayList<Product> cart = new ArrayList<Product>();
    public Connection conn;
    
    // Cart constructor
    public Cart (String conn_string) {
        try {
            Class.forName("org.postgresql.Driver"); 
            conn = DriverManager.getConnection(conn_string,"superuser","Superuser1");
            
        } catch (Exception e) {
                System.out.println(e.toString());
        }
    }
    
    public void addToCart(Product prod) {
    	int idx = cart.indexOf(prod);
    	if (idx != -1) {
    		cart.get(idx).qty = cart.get(idx).qty + prod.qty;
    	} else {
    		cart.add(prod);
    	}
    	
    }
    public void reset() {
        cart.clear();
    }
    
    public int purchase(String customer_name) throws Exception {
    	if (cart.isEmpty())
    		throw new Exception("Your cart is empty!");
        Statement insert_product_order;
        PreparedStatement insert_item;

        int product_id = -1;
        try {
   			conn.setAutoCommit(false);
            insert_product_order = conn.createStatement();
            insert_product_order.execute("INSERT INTO PRODUCT_ORDERS (customer,purchase_date) VALUES ('" +  customer_name+ "','" + new Timestamp(System.currentTimeMillis()) + "')",Statement.RETURN_GENERATED_KEYS);
            
            ResultSet rs = insert_product_order.getGeneratedKeys();
            rs.next();
            product_id = rs.getInt("id");
            
            System.out.println("SIZE:" + cart.size());
            for (int i = 0 ; i < cart.size() ; i++) {
                insert_item = conn.prepareStatement("INSERT INTO PRODUCTS_PURCHASED (product_id,po_number,price,qty) VALUES (?,?,?,?)");
                                
                insert_item.setInt(1,cart.get(i).id);
                insert_item.setInt(2,product_id);
                insert_item.setFloat(3, cart.get(i).price);
                insert_item.setInt(4, cart.get(i).qty);
                try {
                	insert_item.execute();
                } catch (Exception e) {
                	cart.remove(i);
                	throw new Exception("A product was deleted. Please re-try buying your cart");
                }
            }
        } catch (Exception e) { throw e; }
        finally {
		    conn.commit();
		    conn.setAutoCommit(true);
		}   
                
        return product_id;
    }
    
}
