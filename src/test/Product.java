package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Christopher
 */
public class Product {
    public String name;
    public int sku;
    public String category;
    public float price;
    public int qty;
    public int id;
    
    public Product() {}
    public Product(HttpServletRequest req) throws Exception {
    	if (Integer.parseInt(req.getParameter("qty")) <= 0)
    			throw new Exception("Quantity must be greater than 0");
    	Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver"); 
            conn = DriverManager.getConnection("jdbc:postgresql://testdb.ce0dfgtzooky.us-west-2.rds.amazonaws.com:5432/kickass_135","superuser","Superuser1");
        } catch (Exception e) {
                System.out.println(e.toString());
        }
    	
        ResultSet res;
        String test = req.getParameter("id");
        res = conn.createStatement().executeQuery("SELECT * FROM PRODUCTS WHERE id = " + req.getParameter("id"));

    	if (res.next()) {
    		this.id = Integer.parseInt(req.getParameter("id"));
	        this.name =  res.getString("name");
	        this.sku = res.getInt("sku"); 
	        this.category = res.getString("category");
	        this.price = res.getFloat("price");
	        this.qty = Integer.parseInt(req.getParameter("qty"));
	        conn.close();
	        return;
    	}
    	else {
    		conn.close();
    		throw new Exception("I'm sorry, this product no longer exists.");
    	}
    }
    
    public boolean equals(Object other) {
    	if (other instanceof Product) {
    		Product op = (Product)other;
    		if (((Product) other).id == this.id)
    			return true;
    	}
    	
    	return false;
    }
}
